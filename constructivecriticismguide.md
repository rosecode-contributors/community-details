<h1>ROSECODE constructive criticism guide</h1>
<h3>by Brian Whetten</h3>

<p>(Note: Artist in this document and other ROSECODE documents refers to any contributors who submit anything to the platform besides code)</p>

Since ROSECODE is a collaborative project with many people of different skill levels, educations, and talents, we as project maintainers believe that it important to address how to offer criticism in ways that will help other artists improve and grow with the project.

Criticism is both a skill to give with accuracy and politeness, and a skill to receive with grace and understanding. It is a rare instance in the English language where the burden of understanding is on both speaker and listener.

As such, our greatest rule with criticism is to assume good intentions: both as recipient and giver. We do have a strictly enforced code of conduct which provides protections to publishing Artists from extreme behavior such as threats or Trolling (please refer to our code of conduct for other questions). In that regard, it is a safe space. However criticism may feel like an attack, even if it doesn't contain anything directed personally at you.

We want ROSECODE for years to come to be the place where artists of all trades who want to develop their talents flock to. Developing skills and talents as an artist often has as much to do with individual and personal drive as it does what kinds of criticism you're willing to hear and accept.

<q>Remember: It costs nothing to encourage an artist, and the potential benefits are staggering. A pat on the back to an artist now could one day result in your favorite film, or the cartoon you love to get stoned watching, or the song that saves your life. Discourage an artist, you get absolutely nothing in return, ever. I’ve spent the better part of my career getting up after movies and encouraging potential artists in the audience to give it a shot, pointing to myself as proof that anybody can make their dreams come true. I don’t do this altruistically: I’m selfishly insuring that I have cool shit to watch one day by encouraging anybody to follow passions like film or storytelling</q>


<p>Let's go over some common criticism strategies and why criticism is important.<p>

<h2>Why criticize or get criticism?</h2>

<p>As artists, when we've put in dozens of hours on our works, pondered endlessly on the design and composition: the last thing we'll want to hear is what we did wrong with something. Creativity is such an emotionally involved process, and we often treat our work like it was our own child. Yet to improve, we need to have others or ourselves point out the flaws in our work, or at least discuss the flaws with us so that we can improve. Critics aren't only the people on rotten tomatoes that tell us a movie we love is awful, in fact criticism is involved much earlier at every creative step for a film. In fact, directors are just a specialized critic who guides everyone along their shared vision for what the film can be.

<h2>The Shit Sandwich</h2>

For most people in the arts, this is often the first technique taught on how to give criticism. What you do first is point out something they did well, point out what needs touching up or could be improved upon, and then close with another thing they've done well. This can help soften the blow of a potentially devastating reveal of a flaw in a piece, while still giving the artist a chance to improve in their next work or commit to the collaborative work. For artists just barely learning how to improve or take criticism, this is a great place to start.

<p>I want you to notice in the last paragraph the necessity of the pointing out of a flaw or place for improvement. This is a very encouraging form of criticism.

<b>Examples</b>
<p>I really enjoyed the realistic lighting on the hair. The facial proportions aren't quite accurate, but you're getting there.</p>

<p>You've gotten so much better since last time. I can tell you're still working on your story structure, but you keep getting better.</p>

<h2>Specificity and Nitpicking</h2>

<p>When I was our most advanced high school choir, we had an absolutely fantastic director. Since we were often pressed for time in deadlines for concerts or other performances, we often didn't have time for the shit sandwich style criticism. Because of that sometimes we would feel like we had a perfectionist taskmaster behind us, which isn't great when you're trying to create something in a group.

<p>All of that changed when he had a meeting with us about why he taught and directed us in the way that he did. He said in his desire to have us be one of the best high school choirs in the country, we really never had the time to receive criticism that stroked our egos. He congratulated us on every micro achievement and achievement during rehearsal, but he told us to look out for the greatest compliment he could give us: High amounts specificity in the corrections he wanted us to include. When he gave us what some would consider nitpicking: he wanted us to know that every time he gave us that criticism there was an entire backlog of effort that brought him to where he could offer that criticism. When we would spend half an hour practicing a few seconds of music to make sure it was perfect, it meant that the rest of the piece up to that point was perfect.

<p>This kind of criticism is tricky, since it can come off to those unexperienced with it as unnecessary or too specific. But it's a masked compliment, not a backhanded one.

<p>This criticism should be reserved for those offering criticism that have a strong working relationship with the artist, since there is usually a lot of build up to receiving and giving tactfully. It also mixes well with an ongoing mentorship since there's other time for uplifting criticism.

<b>Examples</b>
This still really needs to be fixed.
I know you've done [blank] but it won't look good until [blank].

<p>(Side note: Those wanting to work professionally in any art should expect this in a work environment, but shouldn't expect toxicity)</p>

<h2>Encouragement only criticism</h2>

This is one that I would consider the most dangerous kind of criticism. Remember that comments or criticism should be about recognizing the good the artist has brought into the world with their work, and offering suggestions or ideas on how they can improve or think differently.

When all that you say to an artist is how well they've done, you endanger their ability to improve. That's not to say there isn't tact and poise required to deliver constructive criticism, but to only say how well they've done with no vision of the future is incredibly short sighted.

That's not to say that you should never encourage someone, quite the opposite in fact. You should always make sure that an artist knows how much you appreciate the time they put into creating what you've enjoyed consuming.

<h2>The difference between criticism and a personal attack</h2>
<b>
<q>Everyone's a critic</q>
</b>

Criticism is usually a bad word for most people. It brings up memories of times that people were awful to us, or attack us directly. This kind of behavior is not tolerated in the ROSECODE community (again, please refer to the code of conduct)

I'd like to give some examples of criticism that isn't allowed in our community that is quite common on others.

<b>Examples</b>
I can't believe anyone would make anything like this. It sucks and you should never draw again.
You really think anyone would want to read this? What a joke.

Basically just tell anyone anything besides the fact that they've done a good job or ways that they can improve. If you feel like you need additional assistance with a comment you've received, please refer to the code of conduct.
