<h1>Coding Guide</h1>

As any open source project has, I'm proposing a guide for the coding style and structure so we can keep things organized at the outset and allow all kinds of newcomers and coders to contribute.

Writing code that's understandable by a computer is easy. Writing code that's understandable by humans (READ: Maintainable) is a difficult undertaking.

First and foremost is that every single function needs to have a short comment before it describing what the function does. There are few things more frustrating than having to open another document to figure out what a specific function does.

Second, don't give esoteric names to functions or variables. Just don't for reasons listed above.

Third, as of now we're following the Unix design philosophy of having each module in the app doing one thing. Such as having the init module load but not create the app instance/removing the routes from init, and moving them into their own routes module. As time goes on this should make the project easier to extend and maintain.

Last, any new complete modules should be submitted with a test to verify the code does what it needs to. It isn't hard to make sure code runs most of the time, but making sure code functions the way it needs to under 1000's of connections is a more appropriate test. Currently since there are no complete modules, there aren't any tests. But when the database code, user management, version control, etc... are implemented: you can bet that there will be tests to verify functionality.

Along with the requirements from the last section, in terms of code quality control: the master branch is reserved for code that has been verified top to bottom to work completely in terms of database access, webpage rendering, and other functions that are added down the line (such as streaming and version control).
